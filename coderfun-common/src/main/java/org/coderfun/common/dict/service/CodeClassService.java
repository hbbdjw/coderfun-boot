package org.coderfun.common.dict.service;

import org.coderfun.common.dict.entity.CodeClass;

import klg.common.dataaccess.BaseService;

public interface CodeClassService extends BaseService<CodeClass, Long>{

}
